## Pseudo-Spec

* Reads routine file (lives alongside tod file? Would be useful to maintain somewhere that can be backed up)
* Certain tasks have priority
* One of three responses:
    * skip - retry in 40m
    * not done - prompt again in N minutes
    * done - if repeated, prompt again in 60m; else, prompt tomorrow

What language?

* Python
* C
* Shell

How to structure routine file?

```
!priority task <tab> regularity in minutes
normal task <tab> regularity in minutes
```

---

malloc: https://stackoverflow.com/questions/19613752/how-to-properly-malloc-for-array-of-struct-in-c
remove \n from fegts: https://stackoverflow.com/questions/2693776/removing-trailing-newline-character-from-fgets-input
How to split the text? strtok: https://linux.die.net/man/3/strtok

