#include <stdio.h>

#define BONZO_FILES ".bonzofiles"
#define AUDIO_PLAY_COMMAND "afplay"
#define ALARM_FILE "alert.wav"
#define TASKS_FILE ".bonzo"
#define LOG_FILE ".bonzolog"
#define LIST_DELIMITER "==="
#define LIST_DELIMITER_LENGTH strlen(LIST_DELIMITER)
#define TASK_DELIMITER '\t'
#define COMMENT_CHARACTER "#"

#define MAX_ITEMS 127
#define MAX_LENGTH 255
#define MAX_LISTS 10
#define MIN_OFFSET_IN_MINUTES 60
#define TIMESTAMP_LENGTH 11
#define WAIT_TIME_INPUT_LENGTH 5

#define SECONDS_IN_MINUTE 60
#define S_NOT_DONE_DELAY 10 * SECONDS_IN_MINUTE
#define P_NOT_DONE_DELAY 10 * SECONDS_IN_MINUTE
#define N_NOT_DONE_DELAY 40 * SECONDS_IN_MINUTE
#define OPTIONS "[ (d)one / (n)ot / (w)ait / (s)kip ]"
#define RUNNING_TEXT_BASE "Running Bonzo Buddy"

#define STRCMP_MATCH 0


typedef struct task {
  char name[MAX_LENGTH];
  int regularity;
  int startTime;
  int scheduled;
  int skipCount;
  bool priority;
  bool completed;
} task;


void getListNames(char [][MAX_LENGTH], int , char *[]);

void getBonzoFilepath(char [], char*);

FILE *openBonzoFile(char [], char[]);

bool createTask(task *, char [], int);

bool isInSelectedLists(char [], char [][MAX_LENGTH], int);

char *getRunningText(int *);

