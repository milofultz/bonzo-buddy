#include <errno.h>
#include <ncurses.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "bonzo.h"

int main(int argc, char *argv[]) {
  char listNames[MAX_LISTS][MAX_LENGTH];
  getListNames(listNames, argc, argv);

  time_t now = time(NULL);
  int nowTimestamp = now;

  int startingCount = 0;
  int priorityCount = 0;
  int normalCount = 0;

  task *startingTasks = malloc(MAX_ITEMS * sizeof(*startingTasks));
  task *priorityTasks = malloc(MAX_ITEMS * sizeof(*priorityTasks));
  task *normalTasks = malloc(MAX_ITEMS * sizeof(*normalTasks));

  char rawTask[MAX_LENGTH];

  errno = 0;
  FILE *file = openBonzoFile(TASKS_FILE, "r");

  if (file == NULL) {
    printf("An error occurred when loading file '%s'; errno %d.", TASKS_FILE, errno);
    return 1;
  }

  bool shouldAddTask;
  bool inSelectedLists;

  // For each task in the task file,
  while (fgets(rawTask, MAX_LENGTH, file) != NULL) {
    // Remove newline from end of fgets
    rawTask[strcspn(rawTask, "\n")] = 0;

    // If the line is empty or a comment, skip it (this isn't necessary, but
    // reads better)
    if (strlen(rawTask) == 0 || strncmp(rawTask, COMMENT_CHARACTER, 1) == STRCMP_MATCH) {
      continue;
    // Else if line is a list and it is a selected list, ensure the tasks in
    // it are added.
    } else if (strncmp(rawTask, LIST_DELIMITER, LIST_DELIMITER_LENGTH) == STRCMP_MATCH) {
      int listNamesLength = sizeof(listNames) / sizeof(listNames[0]);
      inSelectedLists = isInSelectedLists(rawTask, listNames, listNamesLength);
    // Else if we are currently in an accepted list, add the task to their
    // corresponding task lists
    } else if (inSelectedLists) {
      if (rawTask[0] == '!' && rawTask[1] == '!') {
        // rawTask + 2 to get rid of the leading exclamation points
        shouldAddTask = createTask(&startingTasks[startingCount], rawTask + 2, nowTimestamp);
        if (shouldAddTask) startingCount++;
      } else if (rawTask[0] == '!') {
        // rawTask + 1 to get rid of the leading exclamation point
        shouldAddTask = createTask(&priorityTasks[priorityCount], rawTask + 1, nowTimestamp);
        if (shouldAddTask) priorityCount++;
      } else {
        shouldAddTask = createTask(&normalTasks[normalCount], rawTask, nowTimestamp);
        if (shouldAddTask) normalCount++;
      }
    }
  }

  // Prepare task queueing process
  task *taskQueue[MAX_ITEMS];
  int queueCount = 0;
  task currentTask;
  int notDoneDelay;
  char formattedName[MAX_LENGTH];
  char formattedTime[TIMESTAMP_LENGTH];
  char formattedDate[TIMESTAMP_LENGTH];
  char expandedInputForLog[22];

  bool isStarting;
  bool isPriority;
  int runningDots = 0;

  errno = 0;
  file = openBonzoFile(LOG_FILE, "a");

  if (file == NULL) {
    printf("An error occurred when loading file '%s'; %s.", LOG_FILE, strerror(errno));
    return 1;
  }

  // Add a header for today's date
  strftime(formattedDate, TIMESTAMP_LENGTH, "%F", localtime(&now));
  fprintf(file, "\n%s\n-----------\n", formattedDate);
  fclose(file);

  // Get path to audio file
  char alarmPath[MAX_LENGTH];
  getBonzoFilepath(ALARM_FILE, alarmPath);

  char playAlarm[MAX_LENGTH];
  strcpy(playAlarm, "\0");
  strcat(playAlarm, AUDIO_PLAY_COMMAND);
  strcat(playAlarm, " ");
  strcat(playAlarm, alarmPath);
  strcat(playAlarm, "\0");

  // Initialize ncurses process
  initscr();
  curs_set(0);
  move(1, 1);

  // While user is still running the software,
  while (true) {
    clear();

    now = time(NULL);
    nowTimestamp = now;

    move(1, 1);
    addstr(getRunningText(&runningDots));
    refresh();

    isStarting = true;
    isPriority = true;
    notDoneDelay = S_NOT_DONE_DELAY;
    for (int i = 0; i < startingCount; i++) {
      currentTask = startingTasks[i];
      if (!currentTask.completed && currentTask.startTime < now) {
        taskQueue[queueCount] = &startingTasks[i];
        queueCount++;
      }
    }

    if (queueCount == 0) {
      isStarting = false;
      notDoneDelay = P_NOT_DONE_DELAY;
      for (int i = 0; i < priorityCount; i++) {
        currentTask = priorityTasks[i];
        if (!currentTask.completed && currentTask.startTime < now && currentTask.scheduled < nowTimestamp) {
          taskQueue[queueCount] = &priorityTasks[i];
          queueCount++;
        }
      }
    }

    // If no priority tasks are in the queue, use normal tasks
    if (queueCount == 0) {
      isPriority = false;
      notDoneDelay = N_NOT_DONE_DELAY;
      for (int i = 0; i < normalCount; i++) {
        currentTask = normalTasks[i];
        if (!currentTask.completed && currentTask.startTime < now && currentTask.scheduled < nowTimestamp) {
          taskQueue[queueCount] = &normalTasks[i];
          queueCount++;
        }
      }
    }

    if (queueCount == 0) {
      sleep(1);
      continue;
    }

    FILE *snd;
    if ((snd = popen(playAlarm, "r"))) {
      pclose(snd);
    }

    char punc[2] = ".";
    if (isPriority || isStarting) {
      strncpy(punc, "!", 2);
    }

    for (int i = 0; i < queueCount; i++) {
      formattedName[0] = '\0';
      formattedTime[0] = '\0';

      if (taskQueue[i]->skipCount > 0) {
        sprintf(formattedName, "%s (%i%s)", taskQueue[i]->name, taskQueue[i]->skipCount, punc);
      } else {
        strcpy(formattedName, taskQueue[i]->name);
      }

      strftime(formattedTime, TIMESTAMP_LENGTH, "[%T]", localtime(&now));
      bool validInputReceived = false;
      while (!validInputReceived) {
        validInputReceived = true;
        move(1, 1);
        printw("%s %s %s ", formattedTime, formattedName, OPTIONS);
        refresh();
        char userInput = getch();
        move(1, 1);
        clrtoeol();
        move(3, 1);
        clrtoeol();
        // Get current time only after input
        now = time(NULL);
        nowTimestamp = now;
        switch (userInput) {
          case 'd':
          case 'D':
            strcpy(expandedInputForLog, "done");
            if (taskQueue[i]->regularity == 0) {
              taskQueue[i]->completed = true;
            } else {
              taskQueue[i]->scheduled = nowTimestamp + taskQueue[i]->regularity;
              taskQueue[i]->skipCount = 0;
            }
            break;
          case 'n':
          case 'N':
            strcpy(expandedInputForLog, "not done");
            taskQueue[i]->scheduled = nowTimestamp + notDoneDelay;
            taskQueue[i]->skipCount = taskQueue[i]->skipCount + 1;
            break;
          case 's':
          case 'S':
            strcpy(expandedInputForLog, "skip");
            if (taskQueue[i]->regularity == 0) {
              taskQueue[i]->completed = true;
            } else {
              taskQueue[i]->scheduled = nowTimestamp + taskQueue[i]->regularity;
              taskQueue[i]->skipCount = taskQueue[i]->skipCount + 1;
            }
            break;
          case 'w':
          case 'W':
            errno = 0;
            char *endptr;
            char waitTimeInput[WAIT_TIME_INPUT_LENGTH + 1];
            int waitTime = 0;
            bool firstEntry = true;

            while (firstEntry || waitTimeInput == endptr || (errno != 0 && waitTime == 0)) {
              move(2, 1);
              addstr("How many minutes do you want to wait for? ");
              move(3, 1);

              if (firstEntry == false) {
                move(4, 1);
                addch('?');
                move(3, 1);
              } else {
                firstEntry = false;
              }

              clrtoeol();
              refresh();
              getnstr(waitTimeInput, WAIT_TIME_INPUT_LENGTH);
              waitTime = strtol(waitTimeInput, &endptr, 10);
            }
            sprintf(expandedInputForLog, "wait for %i minutes", waitTime);
            taskQueue[i]->scheduled = nowTimestamp + (waitTime * SECONDS_IN_MINUTE);
            break;
          default:
            move(3, 1);
            addch('?');
            validInputReceived = false;
        }

        if (validInputReceived) {
          file = openBonzoFile(LOG_FILE, "a");
          fprintf(file, "%s %s -> %s\n", formattedTime, formattedName, expandedInputForLog);
          fclose(file);
          expandedInputForLog[0] = '\0';
        }

        clear();
      }
    }

    queueCount = 0;
  }

  return 0;
}

void getListNames(char listNames[][MAX_LENGTH], int argc, char *argv[]) {
  // `always` should always be present
  strcpy(listNames[0], "always");
  for (int i = 1; i < argc; i++) {
    strcpy(listNames[i], argv[i]);
  }
}

void getBonzoFilepath(char filename[], char *bonzopath) {
  strcpy(bonzopath, "\0");
  strcat(bonzopath, getenv("HOME"));
  strcat(bonzopath, "/");
  strcat(bonzopath, BONZO_FILES);
  strcat(bonzopath, "/");
  strcat(bonzopath, filename);
  strcat(bonzopath, "\0");
}

FILE *openBonzoFile(char filename[], char mode[]) {
  char bonzopath[MAX_LENGTH];
  getBonzoFilepath(filename, bonzopath);
  return fopen(bonzopath, mode);
}

bool createTask(task *newTask, char rawTask[], int nowTimestamp) {
  // Make local copy of rawTask
  char currentRawTask[MAX_LENGTH];
  strcpy(currentRawTask, rawTask);

  int globalIndex = 0;
  int index = 0;
  bool atEnd = false;
  char charBuffer;
  char buffer[128];

  // Set task name
  // TODO: Extract this string to buffer logic to function
  while ((charBuffer = currentRawTask[globalIndex])) {
    buffer[index] = charBuffer;
    index++;
    globalIndex++;
    // If end of string, skip past parsing
    if (charBuffer == '\0') {
      atEnd = true;
      break;
    // Else if end of value, set end of value for buffer
    } else if (charBuffer == TASK_DELIMITER) {
      buffer[index - 1] = '\0';
      break;
    }
  }
  buffer[index] = '\0';
  strcpy(newTask->name, buffer);

  // Set task recurring time if exists. Else, set to 0.
  index = 0;
  while ((charBuffer = currentRawTask[globalIndex]) && !atEnd) {
    globalIndex++;
    buffer[index] = charBuffer;
    index++;
    if (charBuffer == '\0') {
      atEnd = true;
      break;
    } else if (charBuffer == TASK_DELIMITER) {
      buffer[index - 1] = '\0';
      break;
    }
  }
  buffer[index] = '\0';
  char *regularity = buffer;
  int regularityInMinutes;
  if (regularity[0] != '\0') {
    regularityInMinutes = atoi(regularity);
  } else {
    // If no token exists or token == NULL, it is a one-time task
    regularityInMinutes = 0;
  }

  // Set task starting time if exists. Else, set to 0.
  index = 0;
  while ((charBuffer = currentRawTask[globalIndex]) && !atEnd) {
    globalIndex++;
    buffer[index] = charBuffer;
    index++;
    if (charBuffer == '\0') {
      atEnd = true;
      break;
    } else if (charBuffer == TASK_DELIMITER) {
      buffer[index - 1] = '\0';
      break;
    }
  }
  buffer[index] = '\0';
  char *startTime = buffer;
  int startTimeEpoch;
  // If startTime is set, convert into a datetime object
  if (startTime[0] != '\0') {
    char hours[3];
    char minutes[3];
    time_t now;
    struct tm now_datetime;

    strncpy(hours, startTime, 2);
    hours[2] = '\0';
    strncpy(minutes, &startTime[2], 2);
    minutes[2] = '\0';

    time(&now);
    now_datetime = *localtime(&now);
    now_datetime.tm_hour = atoi(hours);
    now_datetime.tm_min = atoi(minutes);
    now_datetime.tm_sec = 0;
    startTimeEpoch = (int) mktime(&now_datetime);
  } else {
    // If no token exists/token == NULL, it is a one-time task
    startTimeEpoch = 0;
  }

  // Make timing offset so not all tasks happen at once at first.
  srand(clock());
  int timingOffset = regularityInMinutes > MIN_OFFSET_IN_MINUTES ?
    (rand() % (regularityInMinutes / 2)) * SECONDS_IN_MINUTE :
    (rand() % MIN_OFFSET_IN_MINUTES) * SECONDS_IN_MINUTE;
  newTask->regularity = regularityInMinutes * SECONDS_IN_MINUTE;
  newTask->startTime = startTimeEpoch;
  newTask->scheduled = startTimeEpoch != 0 ? startTimeEpoch : nowTimestamp + timingOffset;
  newTask->skipCount = 0;
  newTask->completed = false;

  // If startTime is not 0 and we are currently beyond the starting time,
  if (startTimeEpoch != 0 && startTimeEpoch < nowTimestamp) {
    // And it is not a recurring task, ensure we don't add the task to
    // the task list
    if (regularityInMinutes == 0) {
      return false;
    // Then set the scheduled time to the next regular time from start
    } else {
      // While scheduled time is before now, move it forward by one interval
      while (newTask->scheduled < nowTimestamp) newTask->scheduled += newTask->regularity;
    }
  }

  // Ensure task gets added to list
  return true;
}

bool isInSelectedLists(char rawText[], char listNames[][MAX_LENGTH], int listNamesLength) {
  // remove delimiters
  char rawListName[MAX_LENGTH];
  sprintf(rawListName, "%s", rawText + LIST_DELIMITER_LENGTH);
  rawListName[strlen(rawListName) - LIST_DELIMITER_LENGTH] = '\0';

  bool invert = rawListName[0] == '!';
  // Remove leading exclamation point
  if (invert) memmove(rawListName, rawListName+1, strlen(rawListName));
  // If matches in list names and '!', it should return `false`
  bool matchResult = !invert;

  // Check if list name is in selected list of names or if explicitly
  // *not* in list of names
  for (int i = 0; i < listNamesLength; i++) {
    if (strcmp(rawListName, listNames[i]) == STRCMP_MATCH) {
      return matchResult;
    }
  }

  return !matchResult;
}

char *getRunningText(int *runningDots) {
  char *runningText = malloc(strlen(RUNNING_TEXT_BASE) + 3);
  strcpy(runningText, RUNNING_TEXT_BASE);
  *runningDots = (*runningDots + 1) % 3;
  switch (*runningDots) {
    case 2:
      strcat(runningText, ".");
    case 1:
      strcat(runningText, ".");
    case 0:
      strcat(runningText, ".");
  }
  return runningText;
}

