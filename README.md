# Bonzo Buddy `✍(◔◡◔)`

I'm just trying to rip off [nowify](https://taylor.town/projects/nowify).

## What It Does

Bonzo prompts you at various points in your day with routines, tasks, and check-ins to help keep you healthy and sane. Using a gentle WAV file and OSX's `afplay`, it shows you the task and asks if you have done it yet, if it isn't done yet, or if you want to skip it.

## Installation

Run `make install` to create a `~/.bonzofiles` folder, your `.bonzo` file, and a `.bonzolog` file. After that, run `make` to create your binary.

## Usage

Run `./bonzo`. If you want to use different lists, you can add each one as an argument following the initial `./bonzo` call. e.g. `./bonzo work @home` will show tasks from `always`, `work`, and `@home`.

## Spec

### `.bonzo` File

Make `.bonzo` file in your `HOME` directory.

Empty lines and lines beginning with a `#` are ignored.

#### Lists

Each list is delimited by three equals preceding and succeeding the list name, e.g. `===work===`.

There are two special cases for lists:

* The list `===always===` is a reserved list that will always be invoked. If you always want certain tasks to run, they must go inside of the `===always===` list.
* The character `!` is a reserved first character for a list name. A list starting with `!` will be invoked if and only if the list name after `!` is not present in the arguments (e.g. `===!work===` will run only if `work` is not in the list of arguments at invocation. Invoked: `./bonzo @home`; Not invoked: `./bonzo work @home`).

#### Tasks

Everything below a list and before another list is a task. Each task is on a separate line. On each line, the following parameters are separated by a tab:

* Task name: prefaced by a `!` if "priority" or `!!` if "starting".
* Regularity: minutes after completion for task to show up again. Use `0` if not a recurring task.
* Starting time: 24-hour time of when the task will be first run, written as `HHMM`. (If Bonzo Buddy is started after this time is passed and it is not a regular task, it will not run; else, it will run at the next regular interval)
* Any line starting with a `#` is considered a comment and will be ignored.

#### Example `.bonzo` File

```
===always===
!Drink a glass of water <tab> 120
Take a deep breath <tab> 60

===@home===
Brush your teeth <tab> <tab> 1300

===work===
!!Get to inbox zero
Complete TPS report <tab> 15 <tab> 1530

===!work===
Drink tea <tab> <tab> 0900
# Go on a walk <tab> 100
```

* The tasks in the `always` list will always run.
    * "Drink a glass of water" is a priority task that will repeat every 2 hours
    * "Take a deep breath" is a normal task that will repeat every hour
* The tasks in the `@home` list will only run if that list name is specified.
    * "Brush your teeth" is a normal task that will first run at 1300 hours and will go away once it is marked "done"
* The tasks in the `work` list will only run if that list name is specified.
    * "Get to inbox zero" is a starting task that will go away once it is marked as "done"
    * "Complete TPS report" is a normal task that will first run at 1530 and then repeat every 15 minutes
* The tasks in the `!work` list will only run if that list name is *not* specified.
    * "Drink tea" is a normal task that will run once at 0900 hours.
    * "Go on a walk" is commented out and will be ignored

### Task Types and Recurrence/Delay Behavior

There are three task types:

* Normal - These tasks will be prompted normally.
* Priority (`!` prefix) - These tasks will be prompted normally with different recurrence/delay behavior.
* Starting (`!!` prefix) - These tasks will be prompted on application start and share Priority recurrence and delay behavior.

| Task Type         | Option   | Delay                                                                          |
|-------------------|----------|--------------------------------------------------------------------------------|
| Starting/Priority | Done     | Repeat after N minutes of "regularity" or never if regularity is 0             |
| "                 | Not Done | 10 minutes                                                                     |
| "                 | Skip     | Prompt again after N minutes of "regularity" or 10 minutes if regularity is 0  |
| Normal            | Done     | Repeat after N minutes of "regularity" or never if regularity is 0             |
| "                 | Not Done | 40 minutes                                                                     |
| "                 | Skip     | Prompt again after N minutes of "regularity" or 40 minutes if regularity is 0  |
| All               | Wait     | Delay for time specified by user in minutes (e.g. `120` will wait for 2 hours) |

## Roadmap

* ~~Keep a log of activity as the session continues. Timestamps in a logfile would work.~~
* ~~Make animation on the "running" line~~
* ~~Make one-time task for review and update your todo list.~~
* ~~Multiple lists that can be invoked via command line. A default which is none, others by name, and maybe an 'all' which go into every list.~~
* ~~Add a "wait" or "pause" option to say to try again in X minutes. Still think maybe an initial delay might be even better, but whatever.~~
* ~~Start time. I want to have something only start prompting after a certain time of day. Maybe a third value signifying hour of day in 24 hour time? (*Note: I added "starting tasks" so now I can ensure things prompt on startup, that's good!*)~~
* ~~Add a comment character for the `bonzo` file.~~
* ~~Make 'skip' act like 'done' but with a different note. It's currently like 'not done', which is inaccurate.~~
* Organize the file better. Make it more logical in it's progression. Separate functions. *Update 20220415: Partly done. Still a bit to do.*
* Keep track between instances? e.g. could stop and start it and it would save all data for the day of what has been done or not
    * if first line matches current strftime, continue appending
    * else, open for writing (make clean slate)
* Break for work pomodoros using threads? Maybe as an exercise to figure out threading, but I don't know. If I did this, I would probably also want to implement a time spreader for when tasks back up, so they don't just keep piling up at the same time.
* Add a test suite. Would love to implement TDD with this, never done it in C. (http://eradman.com/posts/tdd-in-c.html)
* Make it so that keypresses between tasks don't do anything when next task shows up

## Bugs

* I think it is still not recognizing the negation operator for lists. I think it's time to get some tests here.

