P = ${HOME}/.bonzofiles

default:
	gcc -lncurses bonzo.c -o bonzo


install:
	mkdir -p $(P)
	cp alert.wav $(P)
	touch "$(P)/.bonzo"
	touch "$(P)/.bonzolog"

